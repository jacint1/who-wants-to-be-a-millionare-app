export const DATA = [
    {
        question: "Rolex is a company that specializes in what type of product?",
        possibleAnswers: ["Phone",  "Watches",  "Food", "Cosmetic"],
        correctAnswer: "Watches"
    },
    {
        question: "When did the website `Facebook` launch?",
        possibleAnswers: ["2004", "2005", "2006", "2007"],
        correctAnswer: "2004"
    },
    {
        question: "What sort of animal is Walt Disney's Dumbo?",
        possibleAnswers: ["Deer",  "Rabbit",  "Elephant", "Donkey"],
        correctAnswer: "Elephant"
    },
    {
        question: "What name is given to the revolving belt machinery in an airport that delivers checked luggage from the plane to baggage reclaim?",
        possibleAnswers: ["Hangar",  "Terminal",  "Concourse", "Carousel"],
        correctAnswer: "Carousel"
    },
    {
        question: "On February 22, 1989, what group won the first Grammy award for Best Hard Rock/Metal Performance?",
        possibleAnswers: ["Metallica",  "AC/DC",  "Living Colour", "Jethro Tull"],
        correctAnswer: "Jethro Tull"
    },
    {
        question: "Which of these U.S. Presidents appeared on the television series Laugh-In?",
        possibleAnswers: ["Lyndon Johnson",  "Richard Nixon",  "Jimmy Carter", "Gerald Ford"],
        correctAnswer: "Richard Nixon"
    },
    {
        question: "In what language was Anne Frank's original diary first published?",
        possibleAnswers: ["Dutch",  "English",  "French", "German"],
        correctAnswer: "Dutch"
    },
    {
        question: "In what country are all U.S. Major League baseballs currently manufactured?",
        possibleAnswers: ["Costa Rica",  "Haiti",  "Dominican Republic", "Cuba"],
        correctAnswer: "Costa Rica"
    },
    {
        question: "What Shakespeare character says, Something is rotten in the state of Denmark?",
        possibleAnswers: ["Hamlet",  "Marcellus",  "Horatio", "Laertes"],
        correctAnswer: "Marcellus"
    },
    {
        question: "What insect shorted out an early supercomputer and inspired the term computer bug?",
        possibleAnswers: ["Moth",  "Roach",  "Fly", "Japanese beetle"],
        correctAnswer: "Moth"
    },
    {
        question: "Which of the following pieces of currency was the first to use the motto In God We Trust?",
        possibleAnswers: ["Nickel",  "One dolar bill",  "Two-cent piece", "Five dollar bill"],
        correctAnswer: "Two-cent piece"
    },

    {
        question: "Before the American colonies switched to the Gregorian calendar, on what date did their new year start?",
        possibleAnswers: ["March 25",  "July 1",  "September 25", "December 1"],
        correctAnswer: "March 25"
    },
    {
        question: "Which of the following men does not have a chemical element named for him?",
        possibleAnswers: ["Albert Einstein",  "Niels Bohr",  "Isaac Newton", "Enrico Fermi"],
        correctAnswer: "Isaac Newton"
    },
    {
        question: "According to the United Nations, in what year was the world's population half of its present total?",
        possibleAnswers: ["1950",  "1960",  "1970", "1940"],
        correctAnswer: "1960"
    },
    {
        question: "What plant is named after the first U.S. ambassador to Mexico, who brought it to the U.S.?",
        possibleAnswers: ["Fuchsia",  "Juniper",  "Camellia", "Poinsettia"],
        correctAnswer: "Poinsettia"
    },

  ];

export const PRIZEPOOL = ["1.000.000", "500.000", "250.000", "125.000", "64.000", "32.000", "16.000", "8.000", "4.000", "2.000", "1.000","500", "300", "200", "100"]

export const TIME = 30;
