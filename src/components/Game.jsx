import { useState } from "react";
import { PRIZEPOOL, DATA } from "../constants";
import GameOver from "./GameOver";
import Timer from "./Timer";

function Game() {
  const [currentQuestion, setCurrentQuestion] = useState(0);
  const [gameOver, setGameOver] = useState(false);
  const [answerStatus, setAnswerStatus] = useState(null);
  const [selectedAnswer, setselectedAnswer] = useState(null);



  const checkAnswer = (userAnswer) => {
        setselectedAnswer(userAnswer)
        if (userAnswer == DATA[currentQuestion].correctAnswer) {
          setAnswerStatus('correct')
          delay(3000, () => {
            setCurrentQuestion(currentQuestion + 1);
            setAnswerStatus(null)
            setselectedAnswer(null)
            if (!DATA[currentQuestion + 1]) return setGameOver(true);
          })
        } else {
            setAnswerStatus('incorrect')
            delay(3000, () => {
                setGameOver(true);
                setAnswerStatus(null)
                setselectedAnswer(null)
            })
        }
  };

  const delay = (duration, callback) => {
    setTimeout(() => {
      callback();
    }, duration);
  };

  const total = PRIZEPOOL.reverse();
  

  return (
    <div>
      {gameOver || !DATA[currentQuestion] ? (
        <GameOver currentQuestion={currentQuestion} />
      ) : (
        <div className="game">
        <div className="game__timer">
            <Timer setTimeOut={setGameOver} questionNumber={currentQuestion} />
        </div>
          <div className="game__questions">
            <h2 className="game__questions__title">{DATA[currentQuestion].question}</h2>
            <div className="game__questions__buttons">
              {DATA[currentQuestion].possibleAnswers.map((possibleAnswer) => (
                <button
                  key={possibleAnswer}
                  onClick={() => checkAnswer(possibleAnswer)}
                  className={selectedAnswer == possibleAnswer ? answerStatus : ''}
                >
                  {possibleAnswer}
                </button>
              ))}
            </div>
          </div>

          <div className="game__prize">
            {PRIZEPOOL.map((prize, index) => (
              <p className={index === PRIZEPOOL.length - currentQuestion ? 'correct': ''} key={prize}>{prize} $</p>
            ))}
          </div>
        </div>
      )}
    </div>
  );
}

export default Game;
