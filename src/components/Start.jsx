function Start({ setUsername, handleStart }) {
  const handleChange = ({target: {value}}) => setUsername(value);
  

  return (
    <div className="start">
      <input
        placeholder="Enter your name"
        onChange={ handleChange}
      />
      <button onClick={() => handleStart(true)}>
        Start
      </button>
    </div>
  );
}

export default Start