import { PRIZEPOOL } from "../constants";

function GameOver({currentQuestion}) {

    return (
        <div className="game__over">
            <h1>PRIZE WON</h1>
            {
                currentQuestion === 0 ? (
                    <p>0$</p>
                ) :
                (
                    <p>{PRIZEPOOL[(PRIZEPOOL.length-1) - currentQuestion] || PRIZEPOOL[0]} $</p>
                )
            }
        </div>
    );

  }

  export default GameOver