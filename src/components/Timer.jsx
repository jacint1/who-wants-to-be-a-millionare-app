import { useEffect, useState } from "react";
import { TIME } from "../constants"

export default function Timer({ setTimeOut, questionNumber }) {
  const [timer, setTimer] = useState(TIME);

  useEffect(() => {
    if (timer === 0) return setTimeOut(true);
    const interval = setInterval(() => {
      setTimer((prev) => prev - 1);
    }, 1000);
    return () => clearInterval(interval);
  }, [timer, setTimeOut]);

  useEffect(() => {
    setTimer(TIME);
  }, [questionNumber]);
  return timer;
}