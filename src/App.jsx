import "./App.scss";
import { useState } from "react";
import Start from "./components/Start";
import Game from "./components/Game";



function App() {
  const [username, setUsername] = useState();
  const [startGame, setStartGame] = useState(false);

  return (
    <div className="app">
      {!username || !startGame ? (
        <Start setUsername={setUsername}  handleStart={setStartGame}/>
      ) : (
        <>
            <Game />
        </>
      )}
    </div>
  );
}

export default App;